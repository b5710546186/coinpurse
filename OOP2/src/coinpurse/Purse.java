package coinpurse;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.WithdrawStrategy;


/**
 *  A valuable purse contains valuables.
 *  You can insert valuables, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the valuable purse decides which
 *  valuables to remove.
 *  
 *  @author Kaninpat Tangittisak
 */
public class Purse{
	/** Collection of valuables in the purse. */
	
	private List<Valuable> valuables = new ArrayList<Valuable>();
	
	private WithdrawStrategy strategy = new GreedyWithdraw();
	
	/** Capacity is maximum NUMBER of valuables the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;
	
	private final ValueComparator comparator = new ValueComparator();
	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of valuables you can put in purse.
	 */
	public Purse( int capacity ) {
		
		this.capacity = capacity;
	}
	/**
	 * Count and return the number of valuables in the purse.
	 * This is the number of valuables, not their value.
	 * @return the number of valuables in the purse
	 */
	public int count() { 
		return valuables.size();
	}
	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance=0;
		for(int i=0; i<valuables.size(); i++){
			balance+=valuables.get(i).getValue();
		}
		return balance;
	}
	/**
	 * Return the capacity of the valuable purse.
	 * @return the capacity
	 */

	public int getCapacity() {
		return capacity;
	}
	/**     
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
	
		return valuables.size()==capacity;
			
	}
	/** 
	 * Insert a valuable into the purse.
	 * The valuable is only inserted if the purse has space for it
	 * and the valuable has positive value.  No worthless valuables!
	 * @param valuable is a Valuable object to insert into purse
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert( Valuable valuable ) {
		
		if(isFull()==false){
			valuables.add(valuable);
			return true;
		}
		else{
			return false;
		}
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Valuables withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		
		Collections.sort(valuables, comparator);
		Collections.reverse(valuables);
		List<Valuable> templist = new ArrayList<Valuable>();
		templist = strategy.withdraw(amount, valuables);
		if ( amount > 0 )
		{	
			return null;
		}

		for(int i = 0 ; i < templist.size(); i++) {
			valuables.remove(templist.get(i));
		}
		
		Valuable[] valuable = new Valuable[templist.size()];
		templist.toArray(valuable);
		return valuable;
	}

	public void setStrategy(WithdrawStrategy strategy) {
		this.strategy = strategy;
	}
	/** 
	 * @return toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 */
	public String toString() {
		
		return String.format("%d valuables with value %.1f",valuables.size(),this.getBalance());
	}

}

