package coinpurse;
/**
 * Abstract Superclass for Valuables.
 * @author Kaninpat Tangittisak
 *
 */
public abstract class AbstractValuable implements Valuable{
	
	/**
	 * Compare valuable by value.
	 * @param obj is another valuable to compare to this one.
	 * @return true if the value are equal.
	 */
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(this.getClass() != obj.getClass()) return false;
		Valuable object = (Valuable) obj;
		if(this.getValue() == object.getValue()) {
			return true;
		}
		return false;	
	}
	/**
	 * Compare valuable by value.
	 * @param obj is a Valuable to compare to this.
	 * @return -1 if this valuable has lower value, 0 if this valuable has same value, 1 if this valuable has greater value.
	 */
	public int compareTo(Valuable obj){
		if(this.getValue() < obj.getValue()) return -1;
		if(this.getValue() == obj.getValue()) return 0;
		else return 1;
	}
}
