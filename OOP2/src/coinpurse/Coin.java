package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Kaninpat Tangittisak
 * @version 2015.01.20
 */
public class Coin extends AbstractValuable{

    /** Value of the coin. */
    private double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }
    /**
     * @return return the value variable.
     */
	public double getValue() {
		return value;
	}
	/**
	 * @return return a string representation of this coin value.
	 */
	public String toString(){
		return String.format("%.0f-Baht coin",this.value);
	}

    
}

