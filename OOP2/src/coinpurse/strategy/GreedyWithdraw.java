package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
/**
 * GreedyWithdraw.
 * @author Kaninpat Tangittisak
 *
 */
public class GreedyWithdraw implements WithdrawStrategy{
	@Override
	/**
	 * GreedyWithdraw.
	 * @param amount is the amount to withdraw, valuables is list of valuables to withdraw.
	 * @return list of Valuable objects for valuables withdrawn.
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> templist = new ArrayList<Valuable>();
		for(int i=0; i<valuables.size(); i++){
			if(amount >= valuables.get(i).getValue()){
				amount-=valuables.get(i).getValue();
				templist.add(valuables.get(i));
			}
		}
		return templist;
	}
}
