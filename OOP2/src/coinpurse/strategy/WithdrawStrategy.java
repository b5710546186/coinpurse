package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
/**
 * Interface that have withdraw method.
 * @author Kaninpat Tangittisak
 *
 */
public interface WithdrawStrategy {

	/**  
	 *  Withdraw the requested amount of money.
	 *  @param amount is the amount to withdraw and valuables is list of valuables to withdraw.
	 *  @return list of Valuable objects for money withdrawn.
	 */
	public List<Valuable> withdraw( double amount, List<Valuable> valuables) ;
}
