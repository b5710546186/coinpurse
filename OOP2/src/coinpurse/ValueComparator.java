package coinpurse;

import java.util.Comparator;

/**
 * 
 * @author Kaninpat Tangittisak
 *
 */
public class ValueComparator implements Comparator<Valuable>{
	
	@Override
	public int compare(Valuable a, Valuable b) {
		if(a.getValue()<b.getValue()) return -1;
		if(a.getValue()==b.getValue()) return 0;
		else return 1;
	}
}
