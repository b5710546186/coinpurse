package coinpurse;
/**
 * Valuable interface that have value.
 * @author Kaninpat Tangittisak
 *
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * @return this method is use to get the value from valuable.
	 */
	public double getValue();
}
