package coinpurse;

public class Test {
	public static void main(String[] args){
		Purse purse = new Purse( 5 );
		Coin coin1 = new Coin( 10 );
		Coupon c = new Coupon("blue");
		System.out.print(purse.insert( coin1 ));
		System.out.println(purse.insert( c ));										
		System.out.println(purse.getBalance());
		System.out.println(purse.count());
		Valuable[] stuff = purse.withdraw(60);
		System.out.println(stuff[0].toString());
		System.out.println(stuff[1].toString());
		System.out.println(purse.getBalance());
	}
}
