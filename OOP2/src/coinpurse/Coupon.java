package coinpurse;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Kaninpat Tangittisak
 *
 */
public class Coupon extends AbstractValuable{
	private String color;
	private Map<String,Double> map;
	
	/**
	 * 
	 * @param color is the color of coupon.
	 */
	public Coupon(String color){
		this.color = color;
		map = new HashMap<String,Double>( );
		map.put("red",100.0);
		map.put("blue",50.0);
		map.put("green",10.0);
	}
	/**
	 * @return return the value variable.
	 */
	public double getValue(){
		color = color.toLowerCase();
		return map.get(color);
	}
	/**
	 * @return return a string representation of this Coupon.
	 */
	public String toString(){
		return String.format("%s coupon", this.color);
	}
}
