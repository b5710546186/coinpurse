package coinpurse;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Kaninpat Tangittisak
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {

    	Purse purse = new Purse(5);
    	
    	purse.setStrategy(new RecursiveWithdraw());
       
    	ConsoleDialog ui = new ConsoleDialog( purse );
      
    	ui.run();
    }
}
