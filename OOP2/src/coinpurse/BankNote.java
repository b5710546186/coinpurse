package coinpurse;

/**
 * 
 * @author Kaninpat Tangittisak
 *
 */
public class BankNote extends AbstractValuable implements Valuable{
	private static int nextSerialNumber = 1000000;
	private double value;
	
	int serial = 0;
	
	/**
	 * Create value for BankNote.
	 * @param value is value of the BankNote.
	 */
	public BankNote(double value){
		this.value = value;
		serial = getNextSerialNumber();
	}
	/**
	 * set the unique serial number.
	 * @return the number of next serial number.
	 */
	public static int getNextSerialNumber(){
		return nextSerialNumber++;
	}
	/**
	 * @return return the value of variable.
	 */
	public double getValue(){
		return value;
	}
	/**
	 * @return return a string representation of this BankNote.
	 */
	public String toString(){
		return String.format("%.0f-Baht Banknote [%d]", this.value,serial);
	}
}
